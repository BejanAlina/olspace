package com.orange.olsnasa


import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.orange.olsnasa.instances.Instances
import timber.log.Timber

/**
 * Created by alina.bejan on 10/19/2019.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        // Enable vector drawables.
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        // Initialize dependency injection.
        Instances.init(this)
        Timber.plant(Timber.DebugTree())
    }
}